<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>

<h2>PROJECT EDIT</h2>

<form action="/project/edit/${project.id}" method="POST">

    <input type="hidden" name="id" value="${project.id}" />

    <p>
        <div>NAME:</div>
        <div><input type="text" name="name" value="${project.name}" /></div>
    </p>

    <p>
        <div>DESCRIPTION:</div>
        <div><input type="text" name="description" value="${project.description}" /></div>
    </p>

    <button type="submit">SAVE PROJECT</button>

</form>

<jsp:include page="../include/_footer.jsp"/>


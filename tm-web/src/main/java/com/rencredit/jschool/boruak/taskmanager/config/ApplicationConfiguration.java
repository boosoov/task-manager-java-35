package com.rencredit.jschool.boruak.taskmanager.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.rencredit.jschool.boruak.taskmanager")
public class ApplicationConfiguration {
}

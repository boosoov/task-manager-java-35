package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @GetMapping("/project/create")
    public String create() throws EmptyLoginException, EmptyNameException, EmptyUserIdException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        projectService.create(user.getId(), "Empty");
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) throws EmptyLoginException, EmptyIdException, EmptyUserIdException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        projectService.deleteOneById(user.getId(), id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) throws EmptyLoginException, EmptyIdException, EmptyUserIdException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        final Project project = projectService.findOneEntityById(user.getId(), id);
        return new ModelAndView("project-edit", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(Project project) throws EmptyLoginException, EmptyProjectException, EmptyUserIdException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        projectService.create(user.getId(), project);
        return "redirect:/projects";
    }

}

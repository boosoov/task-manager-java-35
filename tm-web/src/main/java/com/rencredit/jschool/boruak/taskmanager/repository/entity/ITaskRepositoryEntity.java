package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITaskRepositoryEntity extends IAbstractRepositoryEntity<Task> {

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<Task> deleteAllByUserId(@NotNull String projectId);

    @Nullable
    Task deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

}

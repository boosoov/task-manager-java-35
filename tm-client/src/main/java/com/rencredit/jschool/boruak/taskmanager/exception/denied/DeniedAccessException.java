package com.rencredit.jschool.boruak.taskmanager.exception.denied;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class DeniedAccessException extends AbstractClientException {

    public DeniedAccessException() {
        super("Access denied...");
    }

}

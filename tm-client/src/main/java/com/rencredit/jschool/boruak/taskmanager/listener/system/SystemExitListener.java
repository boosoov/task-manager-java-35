package com.rencredit.jschool.boruak.taskmanager.listener.system;

import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SystemExitListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-e";
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Exit program.";
    }

    @Override
    @EventListener(condition = "@systemExitListener.name() == #event.command")
    public void handle(final ConsoleEvent event){
        System.exit(0);
    }

}

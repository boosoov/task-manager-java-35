package com.rencredit.jschool.boruak.taskmanager.listener.task;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TaskListShowListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks list.";
    }

    @Override
    @EventListener(condition = "@taskListShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws EmptyUserIdException_Exception, DeniedAccessException_Exception, EmptyUserException {
        System.out.println("[LIST TASKS]");

        @NotNull final SessionDTO session = systemObjectService.getSession();
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTaskByUserId(session);
        @NotNull int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.println(index + ". ");
            ViewUtil.showTask(task);
            index++;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}

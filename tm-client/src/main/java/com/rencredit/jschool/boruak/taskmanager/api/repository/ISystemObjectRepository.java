package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.endpoint.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISystemObjectRepository {

    @Nullable
    SessionDTO putSession(@NotNull final SessionDTO session);

    @Nullable
    SessionDTO getSession();

    @Nullable
    SessionDTO removeSession();

    void clearAll();

}

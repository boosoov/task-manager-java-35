package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import org.jetbrains.annotations.NotNull;

public interface IInfoService {

    @NotNull
    String getArgumentList();

    @NotNull
    String getCommandList();

    @NotNull
    String getDeveloperInfo();

    @NotNull
    String getHelp() throws EmptyCommandException;

    @NotNull
    String getSystemInfo();

    @NotNull
    String getVersion();

}

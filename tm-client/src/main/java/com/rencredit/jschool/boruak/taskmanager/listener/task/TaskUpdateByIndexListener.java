package com.rencredit.jschool.boruak.taskmanager.listener.task;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class TaskUpdateByIndexListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIndexListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws EmptyUserIdException_Exception, EmptyTaskException_Exception, IncorrectIndexException_Exception, DeniedAccessException_Exception, EmptyNameException_Exception, EmptyTaskException, EmptyDescriptionException_Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final SessionDTO session = systemObjectService.getSession();
        @NotNull final TaskDTO taskUpdated = taskEndpoint.updateTaskByIndex(session, index, name, description);
        TerminalUtil.showTask(taskUpdated);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}

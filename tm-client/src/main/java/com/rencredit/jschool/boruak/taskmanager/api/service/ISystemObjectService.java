package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.endpoint.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import org.jetbrains.annotations.Nullable;

public interface ISystemObjectService {

    @Nullable
    SessionDTO putSession(@Nullable SessionDTO session) throws EmptySessionException;

    @Nullable
    SessionDTO getSession();

    @Nullable
    SessionDTO removeSession();

    void clearAll();

}
